//
//  VideoApiService.swift
//  Allrites
//
//  Created by Vitalii on 23/02/2021.
//

import Foundation

enum VideoStatusType: String {
    case progress
    case stop
    case completed
}

protocol VideoApiServiceProtocol {
    
    init(apiToken: String, clientId: String, networkService: NetworkServiceProtocol)
    var apiToken: String { get set }
    var clientId: String { get set }
    var video: Video? { get set }
    
    var viewId: Int { get set }
    var viewStart: Int { get set }
    
    func sendPing(completion: ((_ isSuccess: Bool, _ errorMessage: String?) ->())?)
    func getVideoDetails(for videoId: String, completion: ((_ isSuccess: Bool, _ video: Video?, _ errorMessage: String?) ->())?)
    func setViews(with currentTime: Int, duration: Int, videoStatusType: VideoStatusType, completion: (( _ isSuccess: Bool, _ errorMessage: String?)->())?)
    var hasVideoDetails: Bool { get }
    func clearData()
}

final class VideoApiService: VideoApiServiceProtocol {

    private enum Urls {
        static let baseUrl = "https://content.allrites.com/"
        static let baseApiUrl = "https://api.allrites.com/"
        static let setViewsUrl = URL(string: "\(baseApiUrl)views/v1/video")!
        static func configureSendPingUrl(with apiToken: String, and clientId: String) -> URL {
            return URL(string: "\(baseUrl)api/api-activity?token=\(apiToken)&company_id=\(clientId)&device=iOS")!
        }
        static func configureVideoDetailsUrl(with apiToken: String, and videoId: String) -> URL {
            return URL(string: "\(baseUrl)api/video?token=\(apiToken)&video_id=\(videoId)")!
        }
    }
    
    private var networkService: NetworkServiceProtocol
    
    var apiToken: String
    var clientId: String
    var video: Video?
    var viewId: Int = 0
    var viewStart: Int = 0
    
    var hasVideoDetails: Bool {
        return video != nil
    }
    
    required init(apiToken: String, clientId: String, networkService: NetworkServiceProtocol) {
        self.apiToken = apiToken
        self.clientId = clientId
        self.networkService = networkService
    }
    
    func sendPing(completion: ((_ isSuccess: Bool, _ errorMessage: String?) ->())?) {
        let request = URLRequest(url: Urls.configureSendPingUrl(with: self.apiToken, and: self.clientId), method: .post)
        networkService.performApiRequest(request) { (result: Result<ApiResponse<PingResponse>>) in
            switch result {
            case .success(let pingResponse):
                print(pingResponse.entity.status)
                completion?(true, nil)
            case .failure(let error):
                print(error.errorMessage)
                completion?(false, error.errorMessage)
            }
        }
    }
    
    func getVideoDetails(for videoId: String, completion: ((_ isSuccess: Bool, _ video: Video?, _ errorMessage: String?) ->())?) {
        let request = URLRequest(url: Urls.configureVideoDetailsUrl(with: self.apiToken, and: videoId), method: .get)
        networkService.performApiRequest(request) { (result: Result<ApiResponse<VideoResponse>>) in
            switch result {
            case .success(let videoResponse):
                self.video = videoResponse.entity.video
                self.viewId = Int.random(in: 100000000...900000000)
                completion?(true, videoResponse.entity.video, nil)
            case .failure(let error):
                completion?(false, nil, error.errorMessage)
            }
        }
    }
    
    func setViews(with currentTime: Int, duration: Int, videoStatusType: VideoStatusType, completion: (( _ isSuccess: Bool, _ errorMessage: String?)->())?) {
        
        guard let video = self.video else {
            completion?(false, "No Video")
            return
        }
        
        var parameters = SetViewsRequest(id: viewId, videoId: video.videoId, title: video.videoTitle, duration: video.videoDuration, companyId: clientId, device: "ios", viewEnd: currentTime)
        
        
        switch videoStatusType {
        case .progress:
            parameters.viewStart = 0
            self.viewStart = currentTime
            debugPrint("Video Progress")
            debugPrint(parameters)
        case .stop:
            parameters.viewStart = self.viewStart
            debugPrint("Video Stop")
            print(parameters)
        case .completed:
            clearData()
        }
        
        let request = URLRequest(url: Urls.setViewsUrl, method: .post, parameters: parameters, headers: ["Authorization": self.apiToken])
        
        networkService.performApiRequest(request) { (result: Result<ApiResponse<SetViewsResponse>>) in
            
            switch result {
            case .success(let setViewsResponse):
                print(setViewsResponse.entity.bodyJson?.messageId ?? "")
                completion?(true, nil)
            case .failure(let error):
                print(error.errorMessage)
                completion?(false, error.errorMessage)
            }
        }
    }
    
    func clearData() {
        video = nil
        viewId = 0
        viewStart = 0
    }
}

