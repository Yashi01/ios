//
//  NetworkService.swift
//  AllRites
//
//  Created by Vitalii on 23/02/2021.
//

import Foundation

enum Result<T> {
    case success(T)
    case failure(ApiErrorParsable)
}

//MARK: - Enums
enum HTTPMethod: String {
    case get = "GET"
    case post = "POST"
    case put = "PUT"
    case delete = "DELETE"
    case patch = "PATCH"
}

protocol NetworkServiceProtocol {
    func performApiRequest<T>(_ request: URLRequest, completion: @escaping (Result<ApiResponse<T>>) -> Void)
}

protocol ApiErrorParsable {
    var errorMessage: String { get }
}

final class NetworkService: NetworkServiceProtocol {
    
    private let urlSession: URLSession
    private let successRange = 200...299
    private let serverErrorRange = 500...
    
    //MARK: - Lifecycle
    init() {
        urlSession = URLSession(configuration: .default, delegate: nil, delegateQueue: .main)
    }
    
    // MARK: - Private
    func performApiRequest<T>(_ request: URLRequest, completion: @escaping (Result<ApiResponse<T>>) -> Void) {
        
        let dataTask = urlSession.dataTask(with: request) { data, response, error in
            guard let httpUrlResponse = response as? HTTPURLResponse else {
                completion(.failure(NetworkRequestError(error: error)))
                return
            }
            debugPrint("Server Response \(String(data: data ?? Data(), encoding: .utf8) ?? "") with status code \(httpUrlResponse.statusCode)")
            if self.successRange.contains(httpUrlResponse.statusCode) {
                do {
                    let response = try ApiResponse<T>(data: data, httpUrlResponse: httpUrlResponse)
                    completion(.success(response))
                } catch {
                    completion(.failure(NetworkRequestError(error: error)))
                }
            } else if self.serverErrorRange.contains(httpUrlResponse.statusCode) {
                let error = NSError(domain: "Internal Server error", code: httpUrlResponse.statusCode)
                completion(.failure(NetworkRequestError(error: error)))
            } else {
                completion(.failure(ApiError(data: data, httpUrlResponse: httpUrlResponse)))
            }
        }
        dataTask.resume()
    }
}
