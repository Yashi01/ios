//
//  AllRites.swift
//  AllRites
//
//  Created by Vitalii on 23/02/2021.
//

import Foundation
import AVKit

public protocol AllRitesDelegate: class {
    func didFailToStartAnalytics(with error: String)
}

public final class AllRites: NSObject {
    
    private enum PlayerKeyPath: String {
        case timeControlStatus = "timeControlStatus"
        case bufferEmpty = "playbackBufferEmpty"
        case status = "status"
        case bufferFull = "playbackBufferFull"
        case playbackLikelyToKeepUp = "playbackLikelyToKeepUp"
    }
    
    //MARK: - Properties
    private enum CurrentPlayerState {
        case playing
        case paused
        case readyToPlay
    }
    
    //MARK: - Public
    public weak var delegate: AllRitesDelegate?
    
    //MARK: - Private
    private weak var player: AVPlayer?
    private var playerItemContext = 0
    private var videoApiService: VideoApiServiceProtocol
    private var videoIdExtractor: VideoIdExtractorProtocol
    private var currentPlayerState: CurrentPlayerState = .readyToPlay
    private var timer: Timer?
    private var onBufferEmpty: (()->())?
    private var onPausedEvent: (()->())?
    private var onPlayEvent: (()->())?
    
    private var isBufferEmpty: Bool {
        return player?.currentItem?.isPlaybackBufferEmpty ?? true
    }
    
    private var isPlayerPlaying: Bool {
        return player?.rate == 1.0 && player?.error == nil
    }
    
    //MARK: - Lifecycle
    public init(apiToken: String, clientId: String) {
        videoApiService = VideoApiService(apiToken: apiToken, clientId: clientId, networkService: NetworkService())
        videoIdExtractor = VideoIdExtractor()
        super.init()
    }
    
    deinit {
        if let player = player {
            clear(for: player)
        }
    }
    
    private func subscribeForEvents() {
        
        onBufferEmpty = { [unowned self] in
            self.invalidateTimer()
        }
        
        onPausedEvent = { [weak self] in
            guard let self = self else {
                return
            }
            self.currentPlayerState = .paused
            self.invalidateTimer()
            debugPrint("Video is paused.")
            if let currentTime = self.player?.currentTime().seconds, let duration = self.player?.currentItem?.duration.seconds, currentTime.isValid, duration.isValid {
                self.videoApiService.setViews(with: self.format(currentTime: currentTime), duration: self.format(currentTime: duration), videoStatusType: .stop, completion: nil)
            }
        }
        
        onPlayEvent = { [weak self] in
            guard let self = self else {
                return
            }
            self.currentPlayerState = .playing
            debugPrint("Video is playing.")
            self.startTimer()
            if let currentTime = self.player?.currentTime().seconds, let duration = self.player?.currentItem?.duration.seconds, currentTime.isValid, duration.isValid {
                self.videoApiService.setViews(with: self.format(currentTime: currentTime), duration: self.format(currentTime: duration), videoStatusType: .progress, completion: nil)
            }
        }
    }
    
    //MARK: - Configuration
    public func startVideoTracking(forPlayer player: AVPlayer) {
        clearIfNeeded()
        subscribeForEvents()
        self.player = player
        addObservers()
        videoApiService.sendPing(completion: nil)
        getVideoDetails()
    }
    
    private func getVideoDetails() {
        if let videoUrl = (player?.currentItem?.asset as? AVURLAsset)?.url, let videoId = videoIdExtractor.extractVideoId(from: videoUrl) {
            videoApiService.getVideoDetails(for: videoId, completion: { [weak self] (isSuccess, video, errorMessage) in
                guard let self = self else {
                    return
                }
                if isSuccess {
                    self.sendSetViews()
                } else {
                    self.configureErrorState(with: errorMessage ?? "Unknown error")
                }
            })
        } else {
            self.configureErrorState(with: "Video url appears to be wrong")
        }
    }
    
    private func configureErrorState(with errorMessage: String) {
        clearIfNeeded()
        delegate?.didFailToStartAnalytics(with: errorMessage)
    }
    
    private func clearIfNeeded() {
        if let player = player {
            clear(for: player)
            self.player = nil
        }
    }
    
    public override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        guard let keyPath = keyPath else {
            return
        }
        
        let playerKeyPath = PlayerKeyPath(rawValue: keyPath)
        
        if playerKeyPath == .bufferEmpty {
            onBufferEmpty?()
        }
        
        if playerKeyPath == .timeControlStatus, let change = change, let newValue = change[NSKeyValueChangeKey.newKey] as? Int, let oldValue = change[NSKeyValueChangeKey.oldKey] as? Int {
            let oldStatus = AVPlayer.TimeControlStatus(rawValue: oldValue)
            let newStatus = AVPlayer.TimeControlStatus(rawValue: newValue)
            if newStatus != oldStatus {
                if newStatus == .paused, currentPlayerState == .playing, !isBufferEmpty {
                    onPausedEvent?()
                } else if newStatus == .playing, self.isPlayerPlaying, !isBufferEmpty {
                    onPlayEvent?()
                }
            }
        }
        
        if playerKeyPath == .status {
            let status: AVPlayerItem.Status
            if let statusNumber = change?[.newKey] as? NSNumber {
                status = AVPlayerItem.Status(rawValue: statusNumber.intValue)!
            } else {
                status = .unknown
            }
            
            // Switch over status value
            switch status {
            case .readyToPlay:
                startTimer()
                debugPrint("Player item is ready to play.")
                break
            case .failed:
                debugPrint("Player item failed. See error.")
                break
            case .unknown:
                debugPrint("Player item is not yet ready.")
                break
            @unknown default:
                break
            }
        }
    }
    
    private func startTimer() {
        if timer == nil, !isBufferEmpty{
            timer = Timer.scheduledTimer(timeInterval: 30.0, target: self, selector: #selector(setViews), userInfo: nil, repeats: true)
        }
    }
    
    private func invalidateTimer() {
        timer?.invalidate()
        timer = nil
    }
    
    @objc private func setViews() {
        debugPrint("30 seconds event sent")
        sendSetViews()
    }
    
    private func sendSetViews() {
        if self.currentPlayerState == .playing, self.videoApiService.hasVideoDetails, let currentTime =  self.player?.currentTime().seconds, let duration = self.player?.currentItem?.duration.seconds, currentTime.isValid, duration.isValid {
            self.videoApiService.setViews(with: self.format(currentTime: currentTime), duration: self.format(currentTime: duration), videoStatusType: .progress, completion: nil)
        }
    }
    
    private func format(currentTime time: Double) -> Int {
        return Int(time) * 1000
    }
    
    private func addObservers() {
        player?.currentItem?.addObserver(self, forKeyPath: "status",
                                         options: [.old, .new],
                                         context: &self.playerItemContext)
        player?.currentItem?.addObserver(self, forKeyPath: "playbackBufferEmpty", options: [.old, .new], context: nil)
        player?.addObserver(self, forKeyPath: "timeControlStatus", options: [.old, .new, .initial, .prior], context: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(playerDidFinishPlaying), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: self.player?.currentItem)
    }
    
    @objc private func playerDidFinishPlaying() {
        currentPlayerState = .readyToPlay
        invalidateTimer()
    }
    
    private func clear(for player: AVPlayer) {
        player.currentItem?.removeObserver(self, forKeyPath: "playbackBufferEmpty", context: nil)
        player.currentItem?.removeObserver(self, forKeyPath: "status", context: &self.playerItemContext)
        player.removeObserver(self, forKeyPath: "timeControlStatus", context: nil)
        videoApiService.clearData()
        currentPlayerState = .readyToPlay
    }
}
