//
//  VideoIdExtractor.swift
//  AllRites
//
//  Created by Vitalii on 10/03/2021.
//

import Foundation

protocol VideoIdExtractorProtocol {
    func extractVideoId(from url: URL) -> String?
    var regex: String { get set }
}

final class VideoIdExtractor: VideoIdExtractorProtocol {
    
    var regex: String = "\\/([0-9]{1,6})\\/"
    
    func extractVideoId(from url: URL) -> String? {
        
        if let range = url.absoluteString.range(of: regex, options: .regularExpression) {
            let subString = url.absoluteString[range]
            return String(subString).replacingOccurrences(of: "/", with: "")
        } else {
            return nil
        }
    }
}
