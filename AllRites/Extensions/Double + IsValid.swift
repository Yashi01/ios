//
//  Double + IsValid.swift
//  AllRites
//
//  Created by Vitalii on 26/02/2021.
//

import Foundation

extension Double {
    
    var isValid: Bool {
        !self.isNaN
    }
}
