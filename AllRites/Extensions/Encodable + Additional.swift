//
//  Encodable + Additional.swift
//  AllRites
//
//  Created by Vitalii on 23/02/2021.
//

import Foundation

extension Encodable {
    
    var toDictionary: [String: Any]? {
        
        let jsonEncoder: JSONEncoder = JSONEncoder()
        jsonEncoder.outputFormatting = .prettyPrinted
        
        guard let backToJson: Data = try? jsonEncoder.encode(self),
            let json: Any = try? JSONSerialization.jsonObject(with: backToJson, options: .allowFragments) else {
                
                return nil
        }
        
        return json as? [String: Any]
    }
    
    var toData: Data? {
        
        let jsonEncoder: JSONEncoder = JSONEncoder()
        jsonEncoder.outputFormatting = .prettyPrinted
        
        return try? jsonEncoder.encode(self)
    }
}
