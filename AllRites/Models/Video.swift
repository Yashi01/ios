//
//  Video.swift
//  AllRites
//
//  Created by Vitalii on 24/02/2021.
//

import Foundation

struct Video: Codable {
    
    private enum CodingKeys : String, CodingKey {
        
        case videoId = "id"
        case videoTitle = "title"
        case videoDuration = "duration"
    }
    var videoId: Int
    var videoTitle: String
    var videoDuration: String
}
