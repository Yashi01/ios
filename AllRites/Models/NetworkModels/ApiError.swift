//
//  ApiError.swift
//  AllRites
//
//  Created by Vitalii on 23/02/2021.
//

import Foundation

struct ApiError: ApiErrorParsable {
    let data: Data?
    let httpUrlResponse: HTTPURLResponse
    var errorMessage: String {
        let messageErrorBody = try? JSONDecoder().decode(MessageErrorBody.self, from: data!)
        return messageErrorBody?.message ?? "Unknown error occured"
    }
}
