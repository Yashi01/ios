//
//  PingResponse.swift
//  AllRites
//
//  Created by Vitalii on 24/02/2021.
//

import Foundation

struct PingResponse: Decodable {
    
    //MARK: - Properties
    var status: String
}

