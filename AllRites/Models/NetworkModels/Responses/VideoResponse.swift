//
//  VideoResponse.swift
//  AllRites
//
//  Created by Vitalii on 24/02/2021.
//

import Foundation

struct VideoResponse: Decodable {
    
    //MARK: - Enums
    private enum CodingKeys : String, CodingKey {
        case status
        case video = "data"
    }
    
    //MARK: - Properties
    let status: String?
    let video: Video?
    
}
