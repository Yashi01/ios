//
//  SetViewsResponse.swift
//  AllRites
//
//  Created by Vitalii on 24/02/2021.
//

import Foundation

struct SetViewsResponse: Decodable {
    
    //MARK: - Enums
    private enum CodingKeys : String, CodingKey {
        case bodyJson = "body-json"
    }
    
    //MARK: - Properties
    let bodyJson: SendMessageResult?
}

struct SendMessageResult: Decodable {
    
    //MARK: - Enums
    private enum CodingKeys : String, CodingKey {
        case messageId = "MessageId"
    }
    
    //MARK: - Properties
    let messageId: String?
}
