//
//  NetworkRequestError.swift
//  AllRites
//
//  Created by Vitalii on 23/02/2021.
//

import Foundation

struct NetworkRequestError: ApiErrorParsable {
    let error: Error?
    var errorMessage: String {
        return error?.localizedDescription ?? "Network request error"
    }
}
