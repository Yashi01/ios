//
//  ApiParseError.swift
//  AllRites
//
//  Created by Vitalii on 23/02/2021.
//

import Foundation

struct ApiParseError: Error {
 
    let error: Error
    let httpUrlResponse: HTTPURLResponse
    let data: Data?
    
    var localizedDescription: String {
        return error.localizedDescription
    }
}

