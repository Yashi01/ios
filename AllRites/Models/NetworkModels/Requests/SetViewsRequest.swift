//
//  SetViewsRequest.swift
//  AllRites
//
//  Created by Vitalii on 24/02/2021.
//

import Foundation

struct SetViewsRequest: Codable {
    
    //MARK: - Enums
    private enum CodingKeys : String, CodingKey {
        case id
        case videoId = "video_id"
        case title
        case duration
        case companyId = "company_id"
        case device
        case viewStart = "view_start"
        case viewEnd = "view_end"
        
    }
    
    //MARK: - Properties
    let id: Int
    let videoId: Int
    let title: String
    let duration: String
    let companyId: String
    let device: String
    var viewStart: Int = 0
    var viewEnd: Int
}
