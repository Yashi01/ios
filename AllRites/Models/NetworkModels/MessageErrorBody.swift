//
//  MessageErrorBody.swift
//  AllRites
//
//  Created by Vitalii on 23/02/2021.
//

import Foundation

struct MessageErrorBody: Decodable {
    let message: String
}

