## Installation

To install via CocoaPods add this line to your Podfile.

pod ‘AllRites’, :git => 'git@bitbucket.org:allrites/iossdk.git’, :branch => ‘master’, :tag => ‘1.0’

Run pod install

NOTE: this is a private repo thus to be able to install the framework you must have a generated SSH key.

## Usage

Later on under discussion (how to link the SDK either by Cocoapods or Swift Package Manager, etc)

AllRites

import AllRites

//Create a player you'd like to track
let playerItem = AVPlayerItem(asset: example_url, automaticallyLoadedAssetKeys: set the required keys)
let player = AVPlayer(playerItem: playerItem)

//Initialize AllRites SDK

let allRites = AllRites(apiToken: your_api_token, clientId: your_client_id)

//Subscribe for the delegate to check why video tracking failed
allRites.delegate = self

//Start tracking
allRites.startVideoTracking(forPlayer: player)

let avPlayerViewController = AVPlayerViewController()
avPlayerViewController.player = player

present(avPlayerViewController, animated: true) {
    player.play()
}
