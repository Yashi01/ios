//
//  AllritesTests.swift
//  AllritesTests
//
//  Created by Vitalii on 23/02/2021.
//

import XCTest
@testable import AllRites

class AllritesTests: XCTestCase {
    
    var videoIdExtractor: VideoIdExtractor?

    override func setUp() {
        super.setUp()
        self.videoIdExtractor = VideoIdExtractor()
    }
    
    override func tearDown() {
        super.tearDown()
        self.videoIdExtractor = nil
    }
    
    func testSuccessFullVideoIdExtract() {
        let url = URL(string: "https://allrites-caas.s3-ap-southeast-1.amazonaws.com/packages/packaged-january-2021-VDYO/88074/play-88074.m3u8")!
        let videoId = videoIdExtractor?.extractVideoId(from: url)
        XCTAssertNotNil(videoId)
    }
    
    func testFailFullVideoIdExtract() {
        let url = URL(string: "https://allrites-caas.s3-ap-southeast-1.amazonaws.com/packages/packaged-january-2021-VDYO/88074-/play-88074.m3u8")!
        let videoId = videoIdExtractor?.extractVideoId(from: url)
        XCTAssertNil(videoId)
        
        let url1 = URL(string: "https://allrites-caas.s3-ap-southeast-1.amazonaws.com/packages/packaged-january-2021-VDYO/play/88074.m3u8")!
        let videoId1 = videoIdExtractor?.extractVideoId(from: url1)
        XCTAssertNil(videoId1)
        
        let url2 = URL(string: "https://allrites-caas.s3-ap-southeast-1.amazonaws.com/packages/packaged-january-2021-VDYO/h/play/88074")!
        let videoId2 = videoIdExtractor?.extractVideoId(from: url2)
        XCTAssertNil(videoId2)
        
        let url3 = URL(string: "http://www.google.com")!
        let videoId3 = videoIdExtractor?.extractVideoId(from: url3)
        XCTAssertNil(videoId3)
    }
    
    func testVideoIdEquality() {
        let url = URL(string: "https://allrites-caas.s3-ap-southeast-1.amazonaws.com/packages/packaged-january-2021-VDYO/88074/play-88074.m3u8")!
        
        let videoId = videoIdExtractor?.extractVideoId(from: url)
        XCTAssertEqual("88074", videoId)
        
        let url1 = URL(string: "https://allrites-caas.s3-ap-southeast-1.amazonaws.com/packages/packaged-january-2021-VDYO/13491/play-88074.m3u8")!
        
        let videoId1 = videoIdExtractor?.extractVideoId(from: url1)
        XCTAssertEqual("13491", videoId1)
    }
}
