Pod::Spec.new do |s|

s.platform = :ios
s.ios.deployment_target = '12.0'
s.name = "AllRites"
s.summary = "AllRites analytics SDK."
s.requires_arc = true

s.version = "1.0"

s.license = { :type => "MIT", :file => "LICENSE" }

s.author = { "YourName" => "youremail@gmail.com" }

s.homepage = "https://bitbucket.org/Yashi01/workspace/projects/VA"

s.source = { :git => "git@bitbucket.org:allrites/iossdk.git", 
             :tag => "#{s.version}" }

s.framework = "AVKit"

s.source_files = "AllRites/**/*.{swift}"

s.resources = "AllRites/**/*.{png,jpeg,jpg,storyboard,xib,xcassets}"

s.swift_version = "5.0"

end